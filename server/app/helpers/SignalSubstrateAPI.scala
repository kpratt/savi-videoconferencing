package helpers

import play.api.Play.current
import scala.concurrent.ExecutionContext.Implicits.global
import play.api.libs.ws._
import scala.concurrent.Future
import play.api.libs.json._
import models._

object SignalSubstrateAPI{
  def apply(): SignalSubstrateAPI = new SignalSubstrateAPI("0.0.0.0")

  def spawnConference(clients: List[Client]){
    val api = apply()
    for ( conf <- api.createConference("conf1") ) {
      for ( client <- clients ) {
        api.addParticipant(conf, client.name, client.ip)
      }
    }

  }
}

class SignalSubstrateAPI(ip: String, prefixPath: String = "") {

  //to code
  def createConference(name: String): Future[Int] = {
    val holder: WSRequestHolder = WS.url("/conference/")
    val data = Json.obj(
        "conference_id" -> 0
      , "conference_name" -> name
      , "max_participants" -> Int.MaxValue
    )
    holder.post(data).
      map((r:WSResponse) => r.body.toInt)
  }

  def removeConference(confID: Int): Future[Unit] = {
    val holder: WSRequestHolder = WS.url("/conference/"+confID)
    holder.delete().
      map((r:WSResponse) => ())
  }

  def addParticipant(confID: Int, user: String, sip_uri: String): Future[Unit] = {
    val holder: WSRequestHolder = WS.url("/conference/"+confID+"/participants/")
    val data = Json.obj(
        "sip_uri" -> sip_uri
      , "sip_instance_uri" -> sip_uri
      , "user_name" -> user
    )
    holder.post(data).
      map((r:WSResponse) => r.body.toInt)
  }
}
