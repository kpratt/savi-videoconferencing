package controllers

import play.api.mvc._
import models._

object Application extends Controller {

  val clients = List[Client](
    Client(1, "Bob", "0.0.0.0")
  )

  def index = Action { implicit request =>
    Ok(views.html.group(clients, request.flash.get("success")))
  }

  def startConference() = Action {
    Redirect(routes.Application.index).flashing(
      "success" -> "The conference has been initiated."
    )
  }


}
